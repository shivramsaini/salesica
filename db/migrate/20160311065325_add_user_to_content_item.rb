class AddUserToContentItem < ActiveRecord::Migration
  def change
    add_reference :content_items, :user, index: true, foreign_key: true
  end
end
