class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.string :answer
      t.string :choice_a
      t.string :choice_b
      t.string :choice_c
      t.string :choice_d
      t.references :readiness_event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
