class AddAttachmentToContentItem < ActiveRecord::Migration
  def change
    add_column :content_items, :attachment, :string
  end
end
