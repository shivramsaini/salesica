class AddDescriptionToContentItems < ActiveRecord::Migration
  def change
    add_column :content_items, :description, :string
  end
end
