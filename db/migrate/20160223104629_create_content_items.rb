class CreateContentItems < ActiveRecord::Migration
  def change
    create_table :content_items do |t|
      t.string :name
      t.string :url
      t.string :folder
      t.string :type
      t.string :tags

      t.timestamps null: false
    end
  end
end
