class AddPersonaToContentItem < ActiveRecord::Migration
  def change
    add_column :content_items, :persona, :string
  end
end
