class ChangeScheduleTypeInReadinessEvents < ActiveRecord::Migration
  def change
    change_column :readiness_events, :schedule, :datetime
  end
end
