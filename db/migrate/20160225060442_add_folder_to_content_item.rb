class AddFolderToContentItem < ActiveRecord::Migration
  def change
    add_column :content_items, :industry, :string
    add_column :content_items, :buying_stage, :string
    add_column :content_items, :stage, :string
  end
end
