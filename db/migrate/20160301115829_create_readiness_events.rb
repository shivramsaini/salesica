class CreateReadinessEvents < ActiveRecord::Migration
  def change
    create_table :readiness_events do |t|
      t.string :name
      t.references :content_item, index: true, foreign_key: true
      t.string :collaboration_group_name
      t.date :schedule

      t.timestamps null: false
    end
  end
end
