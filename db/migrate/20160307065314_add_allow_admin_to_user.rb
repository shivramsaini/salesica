class AddAllowAdminToUser < ActiveRecord::Migration
  def change
    add_column :users, :allow_admin, :boolean
  end
end
