class AddApprovalWorkflowToReadinessEvent < ActiveRecord::Migration
  def change
    add_column :readiness_events, :approval_workflow, :string
  end
end
