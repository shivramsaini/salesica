class AddPostToReadinessEvent < ActiveRecord::Migration
  def change
    add_column :readiness_events, :post, :string
  end
end
