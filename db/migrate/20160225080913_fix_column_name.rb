class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :content_items, :type, :content_type
  end
end
