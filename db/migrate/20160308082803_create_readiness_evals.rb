class CreateReadinessEvals < ActiveRecord::Migration
  def change
    create_table :readiness_evals do |t|
      t.references :user, index: true, foreign_key: true
      t.references :readiness_event, index: true, foreign_key: true
      t.string :status
      t.integer :attempts
      t.integer :views

      t.timestamps null: false
    end
  end
end
