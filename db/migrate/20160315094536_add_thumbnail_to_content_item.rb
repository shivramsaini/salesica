class AddThumbnailToContentItem < ActiveRecord::Migration
  def change
    add_column :content_items, :thumbnail, :string
  end
end
