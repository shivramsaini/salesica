# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160318114826) do

  create_table "answers", force: :cascade do |t|
    t.string   "given_answer"
    t.string   "correct_answer"
    t.integer  "result"
    t.string   "user_id"
    t.integer  "question_id"
    t.integer  "attempts"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "answers", ["question_id"], name: "index_answers_on_question_id"

  create_table "content_items", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.string   "folder"
    t.string   "content_type"
    t.string   "tags"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "industry"
    t.string   "buying_stage"
    t.string   "stage"
    t.string   "persona"
    t.string   "description"
    t.string   "attachment"
    t.integer  "user_id"
    t.string   "thumbnail"
  end

  add_index "content_items", ["user_id"], name: "index_content_items_on_user_id"

  create_table "faqs", force: :cascade do |t|
    t.string   "question"
    t.string   "tags"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "folders", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "questions", force: :cascade do |t|
    t.string   "name"
    t.string   "answer"
    t.string   "choice_a"
    t.string   "choice_b"
    t.string   "choice_c"
    t.string   "choice_d"
    t.integer  "readiness_event_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "questions", ["readiness_event_id"], name: "index_questions_on_readiness_event_id"

  create_table "readiness_evals", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "readiness_event_id"
    t.string   "status"
    t.integer  "attempts"
    t.integer  "views"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "readiness_evals", ["readiness_event_id"], name: "index_readiness_evals_on_readiness_event_id"
  add_index "readiness_evals", ["user_id"], name: "index_readiness_evals_on_user_id"

  create_table "readiness_events", force: :cascade do |t|
    t.string   "name"
    t.integer  "content_item_id"
    t.string   "collaboration_group_name"
    t.datetime "schedule"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "post"
    t.string   "approval_workflow"
  end

  add_index "readiness_events", ["content_item_id"], name: "index_readiness_events_on_content_item_id"

  create_table "users", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "oauth_token"
    t.string   "refresh_token"
    t.string   "instance_url"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "allow_admin"
    t.string   "profile_image"
  end

end
