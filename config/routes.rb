Rails.application.routes.draw do
  get 'readiness_events/index'

  root 'pages#login' # rout
  get 'home'    => 'pages#home'
  
  get 'newcontent' => 'pages#newcontent'
  post 'pages/savecontent' => 'pages#savecontent'
  post 'addfolder' => 'pages#addfolder'
  post 'geturlinfo' => 'pages#geturlinfo'
  
  get 'viewcontent' => 'pages#viewcontent'
  get 'readinessevent' => 'pages#readinessevent'
  post 'pages/save_readiness_event' => 'pages#save_readiness_event'
  patch 'pages/save_readiness_event' => 'pages#save_readiness_event'
  
  get 'checkreadiness' => 'pages#checkreadiness'
  post 'save_check_readiness' => 'pages#save_check_readiness'
  get 'cancelreadiness' => 'pages#cancelreadiness'
  
  get 'readinessactivity' => 'pages#readinessactivity'
  get 'userlist' => 'pages#userlist'
  get 'faqlist' => 'pages#faqlist'
  post 'pages/savefaq' => 'pages#savefaq'
  
  get 'dashboard' => 'pages#dashboard'
  
  resources :content_items  , only: [:index, :edit, :update, :destroy]
  resources :readiness_events  , only: [:index, :edit, :update, :destroy]
  
  #salesforce authentication
  match 'auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match 'auth/failure', to: redirect('/'), via: [:get, :post]
  match 'signout', to: 'pages#logout', as: 'signout', via: [:get, :post]

  
end
