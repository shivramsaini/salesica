require 'rubygems'
require 'rufus/scheduler'
require 'uri'
require 'net/http'
require 'net/https'
require 'json'

## to start scheduler
scheduler = Rufus::Scheduler.new

## It will run at 0 minute for each hour
scheduler.cron '0 * * * *' do
  puts "0-----start"
  postevents
    
end

## It will run at 30 minute for each hour
scheduler.cron '30 * * * *' do
  puts "30-----start"
  postevents
    
end
  
def postevents
  puts("start------")
  curr_time = DateTime.now   
  start_time = curr_time - 5.minutes
  puts curr_time
  puts start_time
  baseUrl = Rails.application.config.base_url
  client = Restforce.new :username => Rails.application.config.username,
    :password       => Rails.application.config.password,
    :security_token => Rails.application.config.security_token,
    :client_id      => Rails.application.config.salesforce_app_id,
    :client_secret  => Rails.application.config.salesforce_app_secret
  
  
  @readiness_events = ReadinessEvent.where("schedule >= ? AND schedule <= ?", start_time , curr_time)
  @readiness_events.each do |event|
    puts event.id
    puts event.schedule
    
    #Post to salesforce
    groupnames = event.collaboration_group_name.split(';')
    query = "SELECT Id,Name From CollaborationGroup WHERE "
    groupnames.each do |p|
      query += "Name ='"+p+"' or "
    end
    query = query.chomp('or ') 
    puts query
    groups = client.query(query)
    link_url = baseUrl + '/checkreadiness?id=' + event.id.to_s
    if groups.size >0
      groups.each do |g|
        client.create('FeedItem',Body: event.post, Title: event.content_item.name,ParentId:g.Id,LinkUrl: link_url)
      end
    end
    
    #post to slack
    uri = URI.parse("http://ec2-54-191-25-122.us-west-2.compute.amazonaws.com/slack/")
    http = Net::HTTP.new(uri.host,uri.port)
    req_body = { "channel" => "selling", "unfurl_media" => "true", "message" => "#{event.post}\n#{link_url}" }.to_json
    req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
    req.body = req_body
    res = http.request(req)
    puts "Response #{res.code} #{res.message}: #{res.body}"
  end
      
  puts '-----end'
end