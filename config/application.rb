require File.expand_path('../boot', __FILE__)

require 'rails/all'



# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Salesica
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true
    config.assets.initialize_on_precompile = false
    
    #sandbox details  
=begin
    config.salesforce_app_id = '3MVG9Y6d_Btp4xp4iyQwKTDcv4a4ms_DAKnYfsTAVQmJvchVdVj1RjfTJyz26VW3bmF6Hq6hqEKa6v_F.ko25'
    config.salesforce_app_secret = '1537647742728299486'
    config.base_url = 'https://rails-tutorial-shivramsaini.c9users.io'
    
    #these are for cron job
    config.username = 'kpshiva@ibirds.com'
    config.password = 'ibirds123'
    config.security_token = '473qnNa1eQb5NytwkaP0yQ7nF'
    
=end
    
    #production details
    
    config.salesforce_app_id = '3MVG9uudbyLbNPZP6pO455jUbh.g7IuEWgumYaeIoZCJdXAomULS5VxZylDFL4JufRpI8noDL2TXkCs69t1gT'
    config.salesforce_app_secret = '9098828873961465634'
    config.base_url = 'https://demo.salesica.com'
    
    #these are for cron job
    config.username = 'demo@salesica.com'
    config.password = 'Unknown123'
    config.security_token = 'caIYfb41sRVnXl9MTzLOXALie'
    
  end
end
