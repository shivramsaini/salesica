require 'uri'
require 'net/http'
require 'net/https'
require 'json'
module PagesHelper
  #provides the list of opportunity stages 
  def get_stage_values
   @opp_stage_list = ["Prospecting","Qualification","Needs Analysis","Value Proposition","Id. Decision Makers","Perception Analysis","Proposal/Price Quote","Negotiation/Review","Closed Won","Closed Lost"];
  end
  
  
  #loop through all content items and set vedio_id for each content_item
  def set_vediov_id(content_items)
    content_items.each do |ci|
      if ci.attachment_url.blank? 
        if ci.content_type.blank?
          ci.item_type = 'Video'
        else
          ci.item_type = ci.content_type
        end
      elsif ci.attachment_url.include? ".pdf"
        ci.item_type = 'PDF'
      elsif ci.attachment_url.include? ".ppt"
        ci.item_type = 'PPT'
      end
    end
  end
  
  def set_thumbnail_url(content_items)
    if(!content_items.url.blank?)
      if content_items.url.include? "youtube.com"
        temp_array = content_items.url.split("embed/")
        if temp_array.size > 1
          content_items.thumbnail = "https://img.youtube.com/vi/"+temp_array[1]+"/hqdefault.jpg";
        end
      else content_items.url.include? "vimeo.com"
        tempList = content_items.url.split("video/")
        puts tempList[1]
        if tempList.size > 1
          uri = URI.parse("http://vimeo.com/api/v2/video/"+tempList[1]+".json")
          http = Net::HTTP.new(uri.host,uri.port)
          req = Net::HTTP::Get.new(uri.path)
          res = http.request(req)
          obj = JSON.parse(res.body)
          if obj.size >0
            content_items.thumbnail =  obj[0]["thumbnail_large"]
          end
        end
      end
    end
  end
  
  def make_get_request(req_url)
    url = URI.parse(req_url)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    return eval(res.body)
  end
end
