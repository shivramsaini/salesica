class ContentItemsController < ApplicationController
  before_action :set_content_item, only: [:show, :edit, :update, :destroy]
  include PagesHelper
  
  # GET /content_items
  # GET /content_items.json
  def index
    @content_items = ContentItem.order("created_at DESC")
    set_vediov_id(@content_items)
  end
  


  # GET /content_items/1
  # GET /content_items/1.json
  def show
  end

  # GET /content_items/new
  def new
    @content_item = ContentItem.new
  end

  # GET /content_items/1/edit
  def edit
    @all_folders = Folder.order("created_at ASC")
    get_stage_values   #inside page helper.rb
  end

  # POST /content_items
  # POST /content_items.json
  def create
    @content_item = ContentItem.new(content_item_params)
    if @content_item.save
      flash[:success] = "Content Item Create Successfully , You can create another Content Item."
      redirect_to index
    else
      render 'new'
    end
    
  end

  # PATCH/PUT /content_items/1
  # PATCH/PUT /content_items/1.json
  def update
      set_thumbnail_url( @content_item)
      if @content_item.update(content_item_params)
        redirect_to content_items_url
      else
        @all_folders = Folder.order("created_at ASC")
        get_stage_values   #inside page helper.rb
        render 'edit'
      end
    
  end

  # DELETE /content_items/1
  # DELETE /content_items/1.json
  def destroy
    @content_item.destroy
    respond_to do |format|
      format.html { redirect_to content_items_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content_item
      @content_item = ContentItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def content_item_params
      params.require(:content_item).permit(:name, :url, :folder, :content_type, :tags,:industry,:buying_stage,:stage,:persona,:description)
    end
end
