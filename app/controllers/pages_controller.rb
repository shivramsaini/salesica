require 'uri'
require 'net/http'
require 'net/https'
require 'json'

class PagesController < ApplicationController
  include PagesHelper
  
  #method for login page.
  def login
    @logout_url = ''
  end
  
  #method for logout and redirect to login page.
  def logout
    if current_user
      @logout_url = current_user.instance_url + "/secur/logout.jsp"
    else
      @logout_url=''
    end
    session[:user_id] = nil
    @show_logout_message = true
    render 'login'
  end
  
  
  #method for home page. Shows all content items
  def home
    unless current_user
      redirect_to root_url
    end  
    
    
    @allow_admin = current_user.allow_admin
    if params[:search]
      all_contents = ContentItem.search(params[:search]).order("created_at DESC")
    else
      all_contents = ContentItem.order("created_at DESC")
    end
    
    set_vediov_id(all_contents)
    @content_map = Hash.new
    all_folders = Folder.order("created_at ASC")
    all_folders.each do |fol|
      @content_map[fol.name] = Array.new
    end
    
    all_contents.each do |content|
      existing_content = Array.new
      if !@content_map[content.folder].blank?
        existing_content = @content_map[content.folder]
      end
      existing_content.push content
      @content_map[content.folder] = existing_content
    end
    
  end
  
  
  #method for newcontent page.
  def newcontent
    unless current_user
      redirect_to root_url
    end
    @all_folders = Folder.order("created_at ASC")
    get_stage_values   #inside page helper.rb
    @temp_content_item =  ContentItem.new
    @content_item = ContentItem.new
    @new_folder = Folder.new
    if params[:folder]
      @content_item.folder =params[:folder]
    else  
      @content_item.folder = "Trending Videos";
    end
  end
  
  #method to save content_item reocrd and redirect to home page.
  def savecontent
    unless current_user
      redirect_to root_url
    end
    @temp_content_item =  ContentItem.new
    @content_item = ContentItem.new(content_item_params)
    @content_item.user_id = current_user.id
    set_thumbnail_url( @content_item)
    if @content_item.save
      if !@content_item.attachment_url.blank?
        @content_item.url = request.base_url + @content_item.attachment_url
        @content_item.save!
      end
      redirect_to :action => 'home'
    else
      @all_folders = Folder.order("created_at ASC")
      @new_folder = Folder.new
      get_stage_values   #inside page helper.rb
      render 'newcontent'
    end
  end
  
  #add folder
  def addfolder
    @folder = Folder.new(folder__params)
    folder_names = @folder.name
    folder_list = folder_names.split("\r\n")
    @folders = Array.new
    folder_list.each do |f|
      if !f.blank? && f.strip !=''
        fol = Folder.new
        fol.name = f.strip
        fol.save!
        @folders.push fol
      end
    end
    respond_to do |format|
      format.js   {}
    end
  end
  
  def geturlinfo
    @temp_content_item = ContentItem.new(content_item_params)
    if !@temp_content_item.content_type.blank? 
      type_param = "url."+@temp_content_item.content_type.downcase
    end
    
    if !@temp_content_item.url.blank?
      @tages = make_get_request('http://ec2-54-191-25-122.us-west-2.compute.amazonaws.com/v1/nlp/concepts?query={"type":"'+type_param+'", "subject":"'+@temp_content_item.url+'"}')
      @summary = make_get_request('http://ec2-54-191-25-122.us-west-2.compute.amazonaws.com/v1/nlp/summarize?query={"type":"'+type_param+'", "subject":"'+@temp_content_item.url+'"}')
    end
    respond_to do |format|
      format.js   {}
    end
    
  end
  
  #method to view content_item reocrd and redirect to viewcontent page.
  def viewcontent
    unless current_user
      redirect_to root_url
    end
    @content_item = ContentItem.find(params[:id])
    
  end
  
  
  def readinessevent
    unless current_user
      redirect_to root_url
    end
    @content_item = ContentItem.find(params[:id])
    readiness_event_list = ReadinessEvent.where(content_item:@content_item.id)
    
    if readiness_event_list.size > 0
      @readiness_event = readiness_event_list[0]
      if !@readiness_event.schedule.blank?
        @readiness_event.schedule_date = @readiness_event.schedule.to_date
        @readiness_event.schedule_time = @readiness_event.schedule.hour * 60 + @readiness_event.schedule.min
      end
      q_list = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
      if q_list.size > 0
        que1 = q_list[0]
        
        @readiness_event.Q1 = que1.name 
      	@readiness_event.Q1_a = que1.choice_a 
      	@readiness_event.Q1_b = que1.choice_b 
      	@readiness_event.Q1_c = que1.choice_c 
      	@readiness_event.Q1_d = que1.choice_d 
      	@readiness_event.ans1 = que1.answer 
	
      end
      
      if q_list.size > 1
        que2 = q_list[1]
        @readiness_event.Q2 = que2.name 
      	@readiness_event.Q2_a = que2.choice_a 
      	@readiness_event.Q2_b = que2.choice_b 
      	@readiness_event.Q2_c = que2.choice_c 
      	@readiness_event.Q2_d = que2.choice_d 
      	@readiness_event.ans2 = que2.answer
      end
      
      if q_list.size > 2
        
        
        que3 = q_list[2]
        @readiness_event.Q3 = que3.name 
      	@readiness_event.Q3_a = que3.choice_a 
      	@readiness_event.Q3_b = que3.choice_b 
      	@readiness_event.Q3_c = que3.choice_c 
      	@readiness_event.Q3_d = que3.choice_d 
      	@readiness_event.ans3 = que3.answer
      	
      end
    else
      @readiness_event = ReadinessEvent.new
      @readiness_event.schedule_time = 0;
    end
    
  end
  
  def save_readiness_event
    
    @temp_readiness_event = ReadinessEvent.new(readiness_event__params)
    if !@temp_readiness_event.schedule_date.blank? && !@temp_readiness_event.schedule_time.blank?
      d = Date.parse(@temp_readiness_event.schedule_date)
      t =  @temp_readiness_event.schedule_time.to_i
      @temp_readiness_event.schedule = DateTime.new(d.year,d.month,d.day,t/60,t%60,0)
    end
    @readiness_event = @temp_readiness_event
    @content_item = ContentItem.find(@readiness_event.content_item.id)
    
    if @readiness_event.id.blank?
      
      if @readiness_event.save
        save_questions(true)
        
        redirect_to :action => 'home'
      else
        render 'readinessevent'
      end
    else
      @readiness_event =  ReadinessEvent.find(@readiness_event.id)
      @readiness_event.schedule = @temp_readiness_event.schedule
      if @readiness_event.update(readiness_event__params)
        save_questions(false)
        
        redirect_to :action => 'home'
      else
        render 'readinessevent'
      end
    end
    
  end
  
  def save_questions(is_insert)
    
    que1 = Question.new
    que2 = Question.new
    que3 = Question.new
    if is_insert == false
      q_list = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
      if q_list.size > 0
        que1 = q_list[0]
        
      end
      
      if q_list.size > 1
        que2 = q_list[1]
        
      end
      
      if q_list.size > 2
        que3 = q_list[2]
        
      end
      
    end
    
    
    que1.name = @readiness_event.Q1
    que1.choice_a = @readiness_event.Q1_a
    que1.choice_b = @readiness_event.Q1_b
    que1.choice_c = @readiness_event.Q1_c
    que1.choice_d = @readiness_event.Q1_d
    que1.answer = @readiness_event.ans1
    que1.readiness_event_id = @readiness_event.id
    
    if !que1.name.blank?
      que1.save!
    end
    
    
    
    que2.name = @readiness_event.Q2
    que2.choice_a = @readiness_event.Q2_a
    que2.choice_b = @readiness_event.Q2_b
    que2.choice_c = @readiness_event.Q2_c
    que2.choice_d = @readiness_event.Q2_d
    que2.answer = @readiness_event.ans2
    que2.readiness_event_id = @readiness_event.id
    if !que2.name.blank?
      que2.save!
    end
    
    
    
    que3.name = @readiness_event.Q3
    que3.choice_a = @readiness_event.Q3_a
    que3.choice_b = @readiness_event.Q3_b
    que3.choice_c = @readiness_event.Q3_c
    que3.choice_d = @readiness_event.Q3_d
    que3.answer = @readiness_event.ans3
    que3.readiness_event_id = @readiness_event.id
    if !que3.name.blank?
      que3.save!
    end
    
  end
  
  def create_chatter_post
    
    client = Restforce.new :oauth_token => current_user.oauth_token,
          :refresh_token => current_user.refresh_token,
          :instance_url  => current_user.instance_url,
          :client_id     => Rails.application.config.salesforce_app_id,
          :client_secret => Rails.application.config.salesforce_app_secret
          
      
      
      groupnames = @readiness_event.collaboration_group_name.split(';')
      query = "SELECT Id,Name From CollaborationGroup WHERE "
      groupnames.each do |p|
        query += "Name ='"+p+"' or "
      end
      query = query.chomp('or ') 
      puts query
      groups = client.query(query)
      link_url = request.base_url + '/checkreadiness?id=' + @readiness_event.id.to_s
      if groups.size >0
        groups.each do |g|
          client.create('FeedItem',Body: @readiness_event.post, Title: @content_item.name,ParentId:g.Id,LinkUrl: link_url)
        end
      end
      
      #post to slack
      uri = URI.parse("http://ec2-54-191-25-122.us-west-2.compute.amazonaws.com/slack/")
      http = Net::HTTP.new(uri.host,uri.port)
      req_body = { "channel" => "selling", "unfurl_media" => "true", "message" => "#{@readiness_event.post}\n#{link_url}" }.to_json
      req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
      req.body = req_body
      res = http.request(req)
      puts "Response #{res.code} #{res.message}: #{res.body}"
      
  end
  
  def checkreadiness
    @showresult = false
    @is_error = false
    unless current_user
      session[:ret_url]= request.original_fullpath
      redirect_to root_url
    end
    @readiness_event = ReadinessEvent.find(params[:id])
    @content_item = ContentItem.find(@readiness_event.content_item.id)
    @questions = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
    @answers = Array.new(@questions.size,Answer.new)
  end
  
  def save_check_readiness
    unless current_user
      redirect_to root_url
    end
    user_id=''
    if current_user
      user_id  = current_user.uid
    end
    answers =  params[:answers]
    i=0
    @answers = Array.new
    while i < answers.size do
      @answers.push Answer.new(answer_params(answers[i.to_s]));
      i = i+1
    end
    
    if @answers.size > 0
      question1 = Question.find(@answers[0].question_id)
      @readiness_event = ReadinessEvent.find(question1.readiness_event_id)
      @content_item = ContentItem.find(@readiness_event.content_item.id)
      @questions = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
      
      readiness_id = @readiness_event.id
      @attempts = ReadinessEval.where(readiness_event_id:readiness_id).where(user_id:current_user.id)
      attempt = 0
      if @attempts.size > 0 && !@attempts[0].attempts.blank? && @attempts[0].attempts >2
        @is_error = true
        render 'checkreadiness'
      else
        if @attempts.size >0 && !@attempts[0].attempts.blank?
          attempt = @attempts[0].attempts
        end
        attempt = attempt+1
        i=0
        @points = 0;
        correct=0
        while i < answers.size do
          @answers[i].correct_answer = @questions[i].answer
          @answers[i].user_id = user_id
          @answers[i].attempts = attempt
          if !@answers[i].given_answer.blank? && @answers[i].correct_answer.casecmp(@answers[i].given_answer) == 0
            @answers[i].result = 1
            correct = correct+1
          else
            @answers[i].result = 0
          end
          @answers[i].save
          i = i+1
        end
        @points = (correct * 10 )/ @answers.size
        puts @points
        create_readiness_eval("Completed",attempt)
        @is_error = false
        @showresult = true
        render 'checkreadiness'
      end
    end
  end
  
  #this method is called when user click on cancel button on check readiness screen
  def cancelreadiness
    @readiness_event = ReadinessEvent.find(params[:id])
    create_readiness_eval("In Progress",0)
    redirect_to readinessactivity_path
  end
  
  #this method is for readiness  screen
  def readinessactivity
    user_id= ''
    unless current_user
      redirect_to root_url
    end
    user_id = current_user.uid
    puts user_id
    @current_readiness = ReadinessEvent.order("created_at DESC") 
    
    @current_readiness.each do |readiness|
      score = 0
      readiness.questions.each do |question|
        result = 0
        question.answers.each do |ans|
          if ans.user_id == user_id
            result = ans.result
          end
        end
        score = score+result
      end
      if readiness.questions.size >0
       score = (score * 10 ) / readiness.questions.size
      end
      status = ''
      readiness.readiness_evals.each do |eval|
        puts eval.user.uid
        if eval.user.uid == user_id
          status = eval.status
        end
        puts status
      end
      
      readiness.status = status
      readiness.score = score
      
    end
    
    @current_readiness.each do |read|
      unless read.content_item.url.blank?
        temp_array = read.content_item.url.split("embed/")
        if temp_array.size > 1
          read.content_item.vedio_id = temp_array[1];
        end
      end
    end
    
  end
  
  #this method is to show all users
  def userlist
    @users = User.all
  end
  
  #this method is called from faq screen
  def faqlist
    @faq = Faq.new
    @faq_list = Faq.order("created_at DESC") 
  end
  
  def savefaq
    @faq = Faq.new(faq__params)
    if @faq.save
      redirect_to faqlist_path
    else
      @faq_list = Faq.order("created_at DESC") 
      render 'faqlist'
    end
    
    
  end
  
  
  def create_readiness_eval(status , attempts)
    
    readiness_eval_list = ReadinessEval.where(readiness_event_id:@readiness_event.id).where(user_id:current_user.id)
    readiness_eval= ReadinessEval.new
    
    if readiness_eval_list.size>0
      readiness_eval = readiness_eval_list[0]
    end
    
    if readiness_eval.views.blank?
       readiness_eval.views = 1
    else
      readiness_eval.views = readiness_eval.views + 1
    end
    if readiness_eval.status != 'Completed'
      readiness_eval.status = status
    end
    
    if attempts !=0
      puts attempts
      
      readiness_eval.attempts = attempts
    end
    readiness_eval.readiness_event_id= @readiness_event.id
    readiness_eval.user_id = current_user.id
    
    readiness_eval.save!
    
  end
  
  def dashboard
    @readiness_event = ReadinessEvent.find(params[:id])
    unless @readiness_event.content_item.url.blank?
      temp_array = @readiness_event.content_item.url.split("embed/")
      if temp_array.size > 1
        @readiness_event.content_item.vedio_id = temp_array[1];
      end
    end
    all_users = User.all
    @complete_users = Array.new
    @viewed_users = ReadinessEval.where(readiness_event_id:@readiness_event.id)
    @view_score = 0
    @completed_score = 0
    if all_users.size >0
      @view_score = (@viewed_users.size * 100) / all_users.size
    end
    
    user_score_map = Hash.new
    all_questions = @readiness_event.questions
    if all_questions.size >0
      all_questions.each do |question|
        question.answers.each do |ans|
          puts ans.user_id 
          puts ans.result
          puts ans.question_id
          existing_score = Hash.new
          if !user_score_map[ans.user_id].blank?
            existing_score = user_score_map[ans.user_id]
          end
          existing_score[question.id] = ans.result
          user_score_map[ans.user_id] = existing_score
        end
      end
      puts user_score_map
      @viewed_users.each do |eval|
        if eval.status == 'Completed'
          if !user_score_map[eval.user.uid].blank?
            existing_score = user_score_map[eval.user.uid]
            score = 0
            existing_score.each do |key, value|
              score += value
            end
            score = (score * 10) / all_questions.size
            eval.score = score
            @complete_users.push eval
          end
        end
      end
      
      if @complete_users.size >0
        @completed_score =( @complete_users.size * 100 ) / all_users.size
      end
      
    end
  end
  
  
end




 # Never trust parameters from the scary internet, only allow the white list through.
  def content_item_params
    params.require(:content_item).permit(:name, :url, :folder, :content_type, :tags,:industry,:buying_stage,:stage,:persona,:description,:attachment)
  end
  
  def readiness_event__params
    params.require(:readiness_event).permit(:collaboration_group_name,:post,:schedule_date,:schedule_time,:approval_workflow,:content_item_id,:id,:Q1,:Q1_a,:Q1_b,:Q1_c,:Q1_d,:ans1,:Q2,:Q2_a,:Q2_b,:Q2_c,:Q2_d,:ans2,:Q3,:Q3_a,:Q3_b,:Q3_c,:Q3_d,:ans3)
  end
  
  def answer_params(my_params)
    my_params.permit(:question_id, :given_answer)
  end
  
  def faq__params
    params.require(:faq).permit(:question,:tags)
  end
  def folder__params
    params.require(:folder).permit(:name)
  end
  
  