class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id
    if session[:ret_url].blank?
      redirect_to readinessactivity_path
    else
      returl = session[:ret_url]
      session[:ret_url]=nil
      redirect_to returl
    end
  end

  def destroy
    if current_user
      @logout_url = current_user.instance_url + "/secur/logout.jsp"
    else
      @logout_url=''
    end
    session[:user_id] = nil
    redirect_to root_url
  end
end