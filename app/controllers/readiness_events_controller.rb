class ReadinessEventsController < ApplicationController
  before_action :set_readiness_activity, only: [:edit, :destroy]
  def index
    @rediness_activities = ReadinessEvent.order("created_at DESC")
    @rediness_activities.each do |read|
      unless read.content_item.url.blank?
        temp_array = read.content_item.url.split("embed/")
        if temp_array.size > 1
          read.content_item.vedio_id = temp_array[1];
        end
      end
    end
  end
  
  def edit
    unless current_user
      redirect_to root_url
    end
    if !@readiness_event.schedule.blank?
      @readiness_event.schedule_date = @readiness_event.schedule.to_date
      @readiness_event.schedule_time = @readiness_event.schedule.hour * 60 + @readiness_event.schedule.min
    end
      
    @content_item = ContentItem.find(@readiness_event.content_item.id)
    q_list = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
    
    if q_list.size > 0
      que1 = q_list[0]
      @readiness_event.Q1 = que1.name 
    	@readiness_event.Q1_a = que1.choice_a 
    	@readiness_event.Q1_b = que1.choice_b 
    	@readiness_event.Q1_c = que1.choice_c 
    	@readiness_event.Q1_d = que1.choice_d 
    	@readiness_event.ans1 = que1.answer 
    end
      
    if q_list.size > 1
      que2 = q_list[1]
      @readiness_event.Q2 = que2.name 
    	@readiness_event.Q2_a = que2.choice_a 
    	@readiness_event.Q2_b = que2.choice_b 
    	@readiness_event.Q2_c = que2.choice_c 
    	@readiness_event.Q2_d = que2.choice_d 
    	@readiness_event.ans2 = que2.answer
    end
    
    if q_list.size > 2
      que3 = q_list[2]
      @readiness_event.Q3 = que3.name 
    	@readiness_event.Q3_a = que3.choice_a 
    	@readiness_event.Q3_b = que3.choice_b 
    	@readiness_event.Q3_c = que3.choice_c 
    	@readiness_event.Q3_d = que3.choice_d 
    	@readiness_event.ans3 = que3.answer
    end
    
  end
  
  def update
    @temp_readiness_event = ReadinessEvent.new(readiness_event__params)
    if !@temp_readiness_event.schedule_date.blank? && !@temp_readiness_event.schedule_time.blank?
      d = Date.parse(@temp_readiness_event.schedule_date)
      t =  @temp_readiness_event.schedule_time.to_i
      @temp_readiness_event.schedule = DateTime.new(d.year,d.month,d.day,t/60,t%60,0)
    end
    @content_item = ContentItem.find(@temp_readiness_event.content_item.id)
    
    @readiness_event =  ReadinessEvent.find(@temp_readiness_event.id)
    @readiness_event.schedule = @temp_readiness_event.schedule
    if @readiness_event.update(readiness_event__params)
      save_questions(false)
      redirect_to readiness_events_url
    else
        render 'edit'
    end
  end
  
  def save_questions(is_insert)
    que1 = Question.new
    que2 = Question.new
    que3 = Question.new
    if is_insert == false
      q_list = Question.where(readiness_event:@readiness_event.id).order("created_at ASC") 
      if q_list.size > 0
        que1 = q_list[0]
        
      end
      
      if q_list.size > 1
        que2 = q_list[1]
        
      end
      
      if q_list.size > 2
        que3 = q_list[2]
        
      end
      
    end
    
    
    que1.name = @readiness_event.Q1
    que1.choice_a = @readiness_event.Q1_a
    que1.choice_b = @readiness_event.Q1_b
    que1.choice_c = @readiness_event.Q1_c
    que1.choice_d = @readiness_event.Q1_d
    que1.answer = @readiness_event.ans1
    que1.readiness_event_id = @readiness_event.id
    
    if !que1.name.blank?
      que1.save!
    end
    
    
    
    que2.name = @readiness_event.Q2
    que2.choice_a = @readiness_event.Q2_a
    que2.choice_b = @readiness_event.Q2_b
    que2.choice_c = @readiness_event.Q2_c
    que2.choice_d = @readiness_event.Q2_d
    que2.answer = @readiness_event.ans2
    que2.readiness_event_id = @readiness_event.id
    if !que2.name.blank?
      que2.save!
    end
    
    
    
    que3.name = @readiness_event.Q3
    que3.choice_a = @readiness_event.Q3_a
    que3.choice_b = @readiness_event.Q3_b
    que3.choice_c = @readiness_event.Q3_c
    que3.choice_d = @readiness_event.Q3_d
    que3.answer = @readiness_event.ans3
    que3.readiness_event_id = @readiness_event.id
    if !que3.name.blank?
      que3.save!
    end
    
  	
  end
  
  
  def set_readiness_activity
    @readiness_event = ReadinessEvent.find(params[:id])
  end
  
  def destroy
    @readiness_event.destroy
    redirect_to readiness_events_url
  end
  
end

def readiness_event__params
  params.require(:readiness_event).permit(:collaboration_group_name,:post, :schedule_date,:schedule_time,:approval_workflow,:content_item_id,:id,:Q1,:Q1_a,:Q1_b,:Q1_c,:Q1_d,:ans1,:Q2,:Q2_a,:Q2_b,:Q2_c,:Q2_d,:ans2,:Q3,:Q3_a,:Q3_b,:Q3_c,:Q3_d,:ans3)
end
