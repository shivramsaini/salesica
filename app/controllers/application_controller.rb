class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  def hello
    render text: "Hello"
  end
  
  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user
  
  def get_answer_css(is_answer , given_answer , correct_answer ,choice)
    if is_answer==false
      return ""
    elsif choice.casecmp(correct_answer) == 0 
      return 'green'
    elsif !given_answer.blank? && choice.casecmp(given_answer) == 0 
      return 'red'
    end
  end
  helper_method :get_answer_css
    
end
