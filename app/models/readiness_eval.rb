class ReadinessEval < ActiveRecord::Base
  belongs_to :user
  belongs_to :readiness_event
  
  attr_accessor :score
end
