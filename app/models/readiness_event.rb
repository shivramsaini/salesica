class ReadinessEvent < ActiveRecord::Base
  belongs_to :content_item
  has_many :questions , :dependent => :destroy 
  has_many :readiness_evals ,:dependent => :destroy 
  has_many :answers, :through => :questions
  
  validates :collaboration_group_name, presence: true
  
  attr_accessor :schedule_date
  attr_accessor :schedule_time
  attr_accessor :Q1
  attr_accessor :Q1_a
  attr_accessor :Q1_b
  attr_accessor :Q1_c
  attr_accessor :Q1_d
  attr_accessor :ans1
  
  attr_accessor :Q2
  attr_accessor :Q2_a
  attr_accessor :Q2_b
  attr_accessor :Q2_c
  attr_accessor :Q2_d
  attr_accessor :ans2
  
  attr_accessor :Q3
  attr_accessor :Q3_a
  attr_accessor :Q3_b
  attr_accessor :Q3_c
  attr_accessor :Q3_d
  attr_accessor :ans3
  
  attr_accessor :score
  attr_accessor :status
  
  
end


