class User < ActiveRecord::Base
    def self.from_omniauth(auth)
      
      temp_list = auth.uid.split("/005")
      if temp_list.size > 1
        user_sf_id = "005"+temp_list[1]
      end
      users = User.where(uid:user_sf_id)
      user = User.new
      if users.size > 0
        user = users[0]
      end
        
      user.provider = auth.provider
      user.name = auth.info.name
      user.oauth_token = auth.credentials.token
      user.refresh_token = auth.credentials.refresh_token
      user.instance_url = auth.credentials.instance_url
      user.uid=user_sf_id
        
      client = Restforce.new :oauth_token => user.oauth_token,
        :refresh_token => user.refresh_token,
        :instance_url  => user.instance_url,
        :client_id     => Rails.application.config.salesforce_app_id,
        :client_secret => Rails.application.config.salesforce_app_secret
          
      sf_user = client.query("select UserRole.name,FullPhotoUrl from user where id='"+user_sf_id+"'")
      user_role = ''
      sf_user.each do |u|
        if !u.UserRole.blank?
          user_role =  u.UserRole.Name
        end
        user.profile_image = u.FullPhotoUrl
      end
      if user_role=='CEO'
        user.allow_admin = true
      else
        user.allow_admin = false
      end
      
      
      user.save!
      return user
    end
end
