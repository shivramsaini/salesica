
class ContentItem < ActiveRecord::Base
  belongs_to :user
  attr_accessor :vedio_id
  attr_accessor :item_type
  has_many :readiness_events , :dependent => :destroy
  
  mount_uploader :attachment, AttachmentUploader 
  
  validates :name, presence: true
  validates :folder, presence: true
  
  
  
  scope :including_all_tags, -> (tags) { where(tags.map { |tag| "(tags LIKE '%#{tag}%')" }.join(" AND ")) }
  scope :including_any_tags, -> (tags) { where(tags.map { |tag| "(tags LIKE '%#{tag}%')" }.join(" OR ")) }
  
  
  
  
  
  def self.search(query)
    #production
    where("name ilike ? ", "%#{query}%") 
    #sandbox
    #where("name like ? ", "%#{query}%")
  end
  
  
  def matching_tag_query(tags, condition_separator = 'OR')
    tags.map { |tag| "(tags LIKE '%#{tag}%')" }.join(" #{condition_separator} ")
  end
  
end
