class Question < ActiveRecord::Base
  belongs_to :readiness_event
  has_many :answers , :dependent => :destroy
  attr_accessor :given_answer
end
